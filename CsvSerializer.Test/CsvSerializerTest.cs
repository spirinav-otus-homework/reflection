﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;
using ConsoleApp.Classes;
using NUnit.Framework;

namespace Serializer.Test
{
    [TestFixture]
    public class CsvSerializerTest
    {
        private readonly CultureInfo _enUsCultureInfo = new("en-US");
        private string ExpectedCsvString { get; set; }

        [SetUp]
        public void Init()
        {
            ExpectedCsvString = "";
        }

        [Test]
        public void NotClassSerialization_ThrowSerializationException()
        {
            Assert.Throws<ArgumentException>(() => CsvSerializer.Serialize(null));
            Assert.Throws<SerializationException>(() => CsvSerializer.Serialize(new object()));
            Assert.Throws<SerializationException>(() => CsvSerializer.Serialize(true));
            Assert.Throws<SerializationException>(() => CsvSerializer.Serialize(1));
            Assert.Throws<SerializationException>(() => CsvSerializer.Serialize(1.05));
            Assert.Throws<SerializationException>(() => CsvSerializer.Serialize('\0'));
            Assert.Throws<SerializationException>(() => CsvSerializer.Serialize("string value"));
            Assert.Throws<SerializationException>(() => CsvSerializer.Serialize(new SampleStruct()));
            Assert.Throws<SerializationException>(() => CsvSerializer.Serialize(new List<string>()));
            Assert.Throws<SerializationException>(() => CsvSerializer.Serialize(new string[1]));
        }

        [Test]
        public void ClassFieldPublicPrimitive_SerializeDefaultValues()
        {
            var fieldClass = new ClassFieldPublicPrimitive();
            ExpectedCsvString = "False,0,0,0,0,0,0,0,0,0,0,0,\"\0\",null";
            Assert.AreEqual(ExpectedCsvString, CsvSerializer.Serialize(fieldClass));
        }

        [Test]
        public void ClassFieldPublicPrimitive_SerializeNotDefaultValues()
        {
            var fieldClass = new ClassFieldPublicPrimitive
            {
                BoolField = true,
                ByteField = byte.MaxValue,
                SbyteField = sbyte.MaxValue,
                ShortField = short.MaxValue,
                UshortField = ushort.MaxValue,
                IntField = int.MaxValue,
                UintField = uint.MaxValue,
                LongField = long.MaxValue,
                UlongField = ulong.MaxValue,
                FloatField = float.MaxValue,
                DoubleField = double.MaxValue,
                DecimalField = decimal.MaxValue,
                CharField = char.MaxValue,
                StringField = "string value"
            };
            ExpectedCsvString =
                $"True,{byte.MaxValue},{sbyte.MaxValue},{short.MaxValue},{ushort.MaxValue},{int.MaxValue},{uint.MaxValue},{long.MaxValue},{ulong.MaxValue},{float.MaxValue.ToString(_enUsCultureInfo)},{double.MaxValue.ToString(_enUsCultureInfo)},{decimal.MaxValue.ToString(_enUsCultureInfo)},\"{char.MaxValue}\",\"string value\"";
            Assert.AreEqual(ExpectedCsvString, CsvSerializer.Serialize(fieldClass));
        }

        [Test]
        public void ClassFieldPublicPrimitive_SerializeCharField()
        {
            var fieldClass = new ClassFieldPublicPrimitive();

            fieldClass.CharField = ' ';
            ExpectedCsvString = "False,0,0,0,0,0,0,0,0,0,0,0,\" \",null";
            Assert.AreEqual(ExpectedCsvString, CsvSerializer.Serialize(fieldClass));

            fieldClass.CharField = 'c';
            ExpectedCsvString = "False,0,0,0,0,0,0,0,0,0,0,0,\"c\",null";
            Assert.AreEqual(ExpectedCsvString, CsvSerializer.Serialize(fieldClass));
        }

        [Test]
        public void ClassFieldPublicPrimitive_SerializeStringField()
        {
            var fieldClass = new ClassFieldPublicPrimitive();

            fieldClass.StringField = string.Empty;
            ExpectedCsvString = "False,0,0,0,0,0,0,0,0,0,0,0,\"\0\",\"\"";
            Assert.AreEqual(ExpectedCsvString, CsvSerializer.Serialize(fieldClass));

            fieldClass.StringField = "";
            ExpectedCsvString = "False,0,0,0,0,0,0,0,0,0,0,0,\"\0\",\"\"";
            Assert.AreEqual(ExpectedCsvString, CsvSerializer.Serialize(fieldClass));

            fieldClass.StringField = " ";
            ExpectedCsvString = "False,0,0,0,0,0,0,0,0,0,0,0,\"\0\",\" \"";
            Assert.AreEqual(ExpectedCsvString, CsvSerializer.Serialize(fieldClass));
        }

        [Test]
        public void ClassFieldPrivatePrimitive_SerializeDefaultValues()
        {
            var fieldClass = new ClassFieldPrivatePrimitive();
            ExpectedCsvString = "False,0,0,0,0,0,0,0,0,0,0,0,\"\0\",null";
            Assert.AreEqual(ExpectedCsvString, CsvSerializer.Serialize(fieldClass));
        }

        [Test]
        public void ClassFieldPrivatePrimitive_SerializeNotDefaultValues()
        {
            var fieldClass = new ClassFieldPrivatePrimitive
            (
                true,
                byte.MaxValue,
                sbyte.MaxValue,
                short.MaxValue,
                ushort.MaxValue,
                int.MaxValue,
                uint.MaxValue,
                long.MaxValue,
                ulong.MaxValue,
                float.MaxValue,
                double.MaxValue,
                decimal.MaxValue,
                char.MaxValue,
                "string value"
            );
            ExpectedCsvString =
                $"True,{byte.MaxValue},{sbyte.MaxValue},{short.MaxValue},{ushort.MaxValue},{int.MaxValue},{uint.MaxValue},{long.MaxValue},{ulong.MaxValue},{float.MaxValue.ToString(_enUsCultureInfo)},{double.MaxValue.ToString(_enUsCultureInfo)},{decimal.MaxValue.ToString(_enUsCultureInfo)},\"{char.MaxValue}\",\"string value\"";
            Assert.AreEqual(ExpectedCsvString, CsvSerializer.Serialize(fieldClass));
        }

        [Test]
        public void ClassFieldPrivatePrimitive_SerializeCharField()
        {
            var fieldClass = new ClassFieldPrivatePrimitive();

            fieldClass.SetCharField(' ');
            ExpectedCsvString = "False,0,0,0,0,0,0,0,0,0,0,0,\" \",null";
            Assert.AreEqual(ExpectedCsvString, CsvSerializer.Serialize(fieldClass));

            fieldClass.SetCharField('c');
            ExpectedCsvString = "False,0,0,0,0,0,0,0,0,0,0,0,\"c\",null";
            Assert.AreEqual(ExpectedCsvString, CsvSerializer.Serialize(fieldClass));
        }

        [Test]
        public void ClassFieldPrivatePrimitive_SerializeStringField()
        {
            var fieldClass = new ClassFieldPrivatePrimitive();

            fieldClass.SetStringField(string.Empty);
            ExpectedCsvString = "False,0,0,0,0,0,0,0,0,0,0,0,\"\0\",\"\"";
            Assert.AreEqual(ExpectedCsvString, CsvSerializer.Serialize(fieldClass));

            fieldClass.SetStringField("");
            ExpectedCsvString = "False,0,0,0,0,0,0,0,0,0,0,0,\"\0\",\"\"";
            Assert.AreEqual(ExpectedCsvString, CsvSerializer.Serialize(fieldClass));

            fieldClass.SetStringField(" ");
            ExpectedCsvString = "False,0,0,0,0,0,0,0,0,0,0,0,\"\0\",\" \"";
            Assert.AreEqual(ExpectedCsvString, CsvSerializer.Serialize(fieldClass));
        }

        [Test]
        public void ClassFieldStaticPrimitive_SerializeNotDefaultValues()
        {
            var fieldClass = new ClassFieldStaticPrimitive();
            ExpectedCsvString =
                $"True,{byte.MaxValue},{sbyte.MaxValue},{short.MaxValue},{ushort.MaxValue},{int.MaxValue},{uint.MaxValue},{long.MaxValue},{ulong.MaxValue},{float.MaxValue.ToString(_enUsCultureInfo)},{double.MaxValue.ToString(_enUsCultureInfo)},{decimal.MaxValue.ToString(_enUsCultureInfo)},\"!\",\"string value\"";
            Assert.AreEqual(ExpectedCsvString, CsvSerializer.Serialize(fieldClass));
        }

        [Test]
        public void ClassFieldPrimitiveBase_SerializeNotDefaultValues()
        {
            var fieldClass = new ClassFieldPrimitiveBase();
            ExpectedCsvString =
                $"True,{byte.MaxValue},{sbyte.MaxValue},{short.MaxValue},{ushort.MaxValue},{int.MaxValue},{uint.MaxValue},{long.MaxValue},{ulong.MaxValue},{float.MaxValue.ToString(_enUsCultureInfo)},{double.MaxValue.ToString(_enUsCultureInfo)},{decimal.MaxValue.ToString(_enUsCultureInfo)}";
            Assert.AreEqual(ExpectedCsvString, CsvSerializer.Serialize(fieldClass));
        }

        [Test]
        public void ClassFieldPrimitiveDerivedFirst_SerializeNotDefaultValues()
        {
            var fieldClass = new ClassFieldPrimitiveDerivedFirst();
            ExpectedCsvString =
                $"\"!\",\"string value 1\",True,{byte.MaxValue},{sbyte.MaxValue},{short.MaxValue},{ushort.MaxValue},{int.MaxValue},{uint.MaxValue},{long.MaxValue},{ulong.MaxValue},{float.MaxValue.ToString(_enUsCultureInfo)},{double.MaxValue.ToString(_enUsCultureInfo)},{decimal.MaxValue.ToString(_enUsCultureInfo)}";
            Assert.AreEqual(ExpectedCsvString, CsvSerializer.Serialize(fieldClass));
        }

        [Test]
        public void ClassFieldPrimitiveDerivedSecond_SerializeNotDefaultValues()
        {
            var fieldClass = new ClassFieldPrimitiveDerivedSecond();
            ExpectedCsvString =
                $"\"string value 2\",\"!\",\"string value 1\",True,{byte.MaxValue},{sbyte.MaxValue},{short.MaxValue},{ushort.MaxValue},{int.MaxValue},{uint.MaxValue},{long.MaxValue},{ulong.MaxValue},{float.MaxValue.ToString(_enUsCultureInfo)},{double.MaxValue.ToString(_enUsCultureInfo)},{decimal.MaxValue.ToString(_enUsCultureInfo)}";
            Assert.AreEqual(ExpectedCsvString, CsvSerializer.Serialize(fieldClass));
        }

        [Test]
        public void ClassFieldCollectionBase_ThrowSerializationException()
        {
            Assert.Throws<SerializationException>(() => CsvSerializer.Serialize(new ClassFieldCollectionBase()));
        }

        [Test]
        public void ClassFieldCollectionDerived_ThrowSerializationException()
        {
            Assert.Throws<SerializationException>(() => CsvSerializer.Serialize(new ClassFieldCollectionDerived()));
        }

        [Test]
        public void ClassFieldList_ThrowSerializationException()
        {
            Assert.Throws<SerializationException>(() => CsvSerializer.Serialize(new ClassFieldList()));
        }

        [Test]
        public void ClassFieldDictionary_ThrowSerializationException()
        {
            Assert.Throws<SerializationException>(() => CsvSerializer.Serialize(new ClassFieldDictionary()));
        }

        [Test]
        public void ClassFieldNestedClassCustom_ThrowSerializationException()
        {
            Assert.Throws<SerializationException>(() => CsvSerializer.Serialize(new ClassFieldNestedClassCustom()));
        }

        [Test]
        public void ClassFieldNestedClassSystem_ThrowSerializationException()
        {
            Assert.Throws<SerializationException>(() => CsvSerializer.Serialize(new ClassFieldNestedClassSystem()));
        }

        [Test]
        public void ClassFieldObjectClass_ThrowSerializationException()
        {
            Assert.Throws<SerializationException>(() => CsvSerializer.Serialize(new ClassFieldObjectClass()));
        }

        [Test]
        public void ClassFieldObjectCollection_ThrowSerializationException()
        {
            Assert.Throws<SerializationException>(() => CsvSerializer.Serialize(new ClassFieldObjectCollection()));
        }

        [Test]
        public void ClassFieldObjectPrimitive_SerializeNotDefaultValues()
        {
            var fieldClass = new ClassFieldObjectPrimitive();
            ExpectedCsvString =
                $"True,{byte.MaxValue},{sbyte.MaxValue},{short.MaxValue},{ushort.MaxValue},{int.MaxValue},{uint.MaxValue},{long.MaxValue},{ulong.MaxValue},{float.MaxValue.ToString(_enUsCultureInfo)},{double.MaxValue.ToString(_enUsCultureInfo)},{decimal.MaxValue.ToString(_enUsCultureInfo)},\"{char.MaxValue}\",\"string value\"";
            Assert.AreEqual(ExpectedCsvString, CsvSerializer.Serialize(fieldClass));
        }

        [Test]
        public void ClassFieldStruct_ThrowSerializationException()
        {
            Assert.Throws<SerializationException>(() => CsvSerializer.Serialize(new ClassFieldStruct()));
        }

        [Test]
        public void ClassFieldDateTime_Serialize()
        {
            Assert.AreEqual("01.01.0001 0:00:00", CsvSerializer.Serialize(new ClassFieldDatetime()));
            Assert.AreEqual("31.12.9999 23:59:59",
                CsvSerializer.Serialize(new ClassFieldDatetime {DateTimeField = DateTime.MaxValue}));
        }

        [Test]
        public void ClassPropertyPublicPrimitive_SerializeDefaultValues()
        {
            var fieldClass = new ClassPropertyPublicPrimitive();
            ExpectedCsvString = "False,0,0,0,0,0,0,0,0,0,0,0,\"\0\",null";
            Assert.AreEqual(ExpectedCsvString, CsvSerializer.Serialize(fieldClass));
        }

        [Test]
        public void ClassPropertyPublicPrimitive_SerializeNotDefaultValues()
        {
            var fieldClass = new ClassPropertyPublicPrimitive
            {
                BoolField = true,
                ByteField = byte.MaxValue,
                SbyteField = sbyte.MaxValue,
                ShortField = short.MaxValue,
                UshortField = ushort.MaxValue,
                IntField = int.MaxValue,
                UintField = uint.MaxValue,
                LongField = long.MaxValue,
                UlongField = ulong.MaxValue,
                FloatField = float.MaxValue,
                DoubleField = double.MaxValue,
                DecimalField = decimal.MaxValue,
                CharField = char.MaxValue,
                StringField = "string value"
            };
            ExpectedCsvString =
                $"True,{byte.MaxValue},{sbyte.MaxValue},{short.MaxValue},{ushort.MaxValue},{int.MaxValue},{uint.MaxValue},{long.MaxValue},{ulong.MaxValue},{float.MaxValue.ToString(_enUsCultureInfo)},{double.MaxValue.ToString(_enUsCultureInfo)},{decimal.MaxValue.ToString(_enUsCultureInfo)},\"{char.MaxValue}\",\"string value\"";
            Assert.AreEqual(ExpectedCsvString, CsvSerializer.Serialize(fieldClass));
        }

        [Test]
        public void ClassFieldPrivatePrimitive_DeserializeDefaultValues()
        {
            var fieldClass = new ClassFieldPrivatePrimitive();
            var csvString = CsvSerializer.Serialize(fieldClass);
            var newFieldClass = CsvSerializer.Deserialize<ClassFieldPrivatePrimitive>(csvString);
            Assert.AreEqual(fieldClass.GetHashCode(), newFieldClass.GetHashCode());
        }

        [Test]
        public void ClassFieldPrivatePrimitive_DeserializeNotDefaultValues()
        {
            var fieldClass = new ClassPropertyPublicPrimitive
            {
                BoolField = true,
                ByteField = byte.MaxValue,
                SbyteField = sbyte.MaxValue,
                ShortField = short.MaxValue,
                UshortField = ushort.MaxValue,
                IntField = int.MaxValue,
                UintField = uint.MaxValue,
                LongField = long.MaxValue,
                UlongField = ulong.MaxValue,
                FloatField = float.MaxValue,
                DoubleField = double.MaxValue,
                DecimalField = decimal.MaxValue,
                CharField = char.MaxValue,
                StringField = "string value"
            };
            var csvString = CsvSerializer.Serialize(fieldClass);
            var newFieldClass = CsvSerializer.Deserialize<ClassPropertyPublicPrimitive>(csvString);
            Assert.AreEqual(fieldClass.GetHashCode(), newFieldClass.GetHashCode());
        }
    }
}