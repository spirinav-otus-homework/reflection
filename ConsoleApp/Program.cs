﻿using System;
using System.Diagnostics;
using ConsoleApp.Classes;
using Newtonsoft.Json;
using Serializer;

namespace ConsoleApp
{
    class Program
    {
        private const int IterationNumber = 1000000;

        // ReSharper disable once UnusedParameter.Local
        static void Main(string[] args)
        {
            var results = new string[2];
            var times = new long[3];
            var stopwatch = new Stopwatch();

            var f = new F();
            stopwatch.Start();
            for (var i = 0; i < IterationNumber; i++)
            {
                CsvSerializer.Serialize(f);
            }
            results[0] = CsvSerializer.Serialize(f);
            stopwatch.Stop();
            times[0] = stopwatch.ElapsedMilliseconds;

            var f2 = f.Get();
            stopwatch.Restart();
            for (var i = 0; i < IterationNumber; i++)
            {
                CsvSerializer.Serialize(f2);
            }
            results[1] = CsvSerializer.Serialize(f2);
            stopwatch.Stop();
            times[1] = stopwatch.ElapsedMilliseconds;

            stopwatch.Restart();
            for (var i = 0; i <= IterationNumber; i++)
            {
                CsvSerializer.Deserialize<F>(results[1]);
            }
            times[2] = stopwatch.ElapsedMilliseconds;
            stopwatch.Stop();

            stopwatch.Reset();
            Console.WriteLine($"My serialization, IDE: Visual Studio 2019, {IterationNumber} iterations");
            Console.WriteLine();
            Console.WriteLine($"First serialization result: {results[0]}");
            Console.WriteLine($"First serialization time (ms): {times[0]}");
            Console.WriteLine($"Second serialization result: {results[1]}");
            Console.WriteLine($"Second serialization time (ms): {times[1]}");
            Console.WriteLine($"Difference between first and second serialization (ms): {Math.Abs(times[0] - times[1])}");
            Console.WriteLine($"Deserialization time (ms): {times[2]}");
            stopwatch.Stop();
            Console.WriteLine($"Output to console time (ms): {stopwatch.ElapsedMilliseconds}\n\n");

            Console.WriteLine($"NewtonsoftJson serialization, {IterationNumber} iterations");
            Console.WriteLine();
            stopwatch.Restart();
            for (var i = 0; i < IterationNumber; i++)
            {
                JsonConvert.SerializeObject(f2);
            }
            var json = JsonConvert.SerializeObject(f2);
            stopwatch.Stop();
            Console.WriteLine($"NewtonsoftJson serialization result: {json}");
            Console.WriteLine($"NewtonsoftJson serialization time (ms): {stopwatch.ElapsedMilliseconds}");

            stopwatch.Restart();
            for (var i = 0; i <= IterationNumber; i++)
            {
                JsonConvert.DeserializeObject(json);
            }
            stopwatch.Stop();
            Console.WriteLine($"NewtonsoftJson deserialization time (ms): {stopwatch.ElapsedMilliseconds}");

            Console.ReadLine();
        }
    }
}