﻿// ReSharper disable UnusedMember.Global
namespace ConsoleApp.Classes
{
    /// <summary>
    /// First derived class with fields of primitive type
    /// </summary>
    public class ClassFieldPrimitiveDerivedFirst : ClassFieldPrimitiveBase
    {
#pragma warning disable 414
        private static char _charField = '!';
#pragma warning restore 414
        public string StringFieldFirst = "string value 1";
    }

    /// <summary>
    /// Second derived class with fields of primitive type
    /// </summary>
    public class ClassFieldPrimitiveDerivedSecond : ClassFieldPrimitiveDerivedFirst
    {
        public string StringFieldSecond = "string value 2";
    }
}