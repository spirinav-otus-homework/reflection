﻿// ReSharper disable UnusedMember.Global

namespace ConsoleApp.Classes
{
    /// <summary>
    /// Base class with static/private/public fields of primitive type
    /// </summary>
    public class ClassFieldPrimitiveBase
    {
#pragma warning disable 414
        private static bool _boolField = true;
#pragma warning restore 414
        public static byte ByteField = byte.MaxValue;
        protected static sbyte SbyteField = sbyte.MaxValue;
        internal static short ShortField = short.MaxValue;
        private protected static ushort UshortField = ushort.MaxValue;
        protected internal static int IntField = int.MaxValue;

#pragma warning disable 414
        private uint _uintField = uint.MaxValue;
#pragma warning restore 414
        public long LongField = long.MaxValue;
        protected ulong UlongField = ulong.MaxValue;
        internal float FloatField = float.MaxValue;
        private protected double DoubleField = double.MaxValue;
        protected internal decimal DecimalField = decimal.MaxValue;
    }
}