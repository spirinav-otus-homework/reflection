﻿// ReSharper disable NotAccessedField.Local
namespace ConsoleApp.Classes
{
    /// <summary>
    /// Сlass with private fields of primitive type
    /// </summary>
    public class ClassFieldPrivatePrimitive
    {
        private bool _boolField;
        private byte _byteField;
        private sbyte _sbyteField;
        private short _shortField;
        private ushort _ushortField;
        private int _intField;
        private uint _uintField;
        private long _longField;
        private ulong _ulongField;
        private float _floatField;
        private double _doubleField;
        private decimal _decimalField;
        private char _charField;
        private string _stringField;

        public ClassFieldPrivatePrimitive()
        {
        }

        public ClassFieldPrivatePrimitive(
            bool boolField,
            byte byteField,
            sbyte sbyteField,
            short shortField,
            ushort ushortField,
            int intField,
            uint uintField,
            long longField,
            ulong ulongField,
            float floatField,
            double doubleField,
            decimal decimalField,
            char charField,
            string stringField)
        {
            _boolField = boolField;
            _byteField = byteField;
            _sbyteField = sbyteField;
            _shortField = shortField;
            _ushortField = ushortField;
            _intField = intField;
            _uintField = uintField;
            _longField = longField;
            _ulongField = ulongField;
            _floatField = floatField;
            _doubleField = doubleField;
            _decimalField = decimalField;
            _charField = charField;
            _stringField = stringField;
        }

        public void SetCharField(char charValue)
        {
            _charField = charValue;
        }

        public void SetStringField(string stringValue)
        {
            _stringField = stringValue;
        }

        public override int GetHashCode()
        {
            return _boolField.GetHashCode() +
                   _byteField.GetHashCode() +
                   _sbyteField.GetHashCode() +
                   _shortField.GetHashCode() +
                   _ushortField.GetHashCode() +
                   _intField.GetHashCode() +
                   _uintField.GetHashCode() +
                   _longField.GetHashCode() +
                   _ulongField.GetHashCode() +
                   _floatField.GetHashCode() +
                   _doubleField.GetHashCode() +
                   _decimalField.GetHashCode() +
                   _charField.GetHashCode() +
                   (_stringField?.GetHashCode() ?? 0);
        }
    }
}