﻿// ReSharper disable UnusedMember.Global
namespace ConsoleApp.Classes
{
    /// <summary>
    /// Сlass with public property of primitive type
    /// </summary>
    public class ClassPropertyPublicPrimitive
    {
        public bool BoolField { get; set; }
        public byte ByteField { get; set; }
        public sbyte SbyteField { get; set; }
        public short ShortField { get; set; }
        public ushort UshortField { get; set; }
        public int IntField { get; set; }
        public uint UintField { get; set; }
        public long LongField { get; set; }
        public ulong UlongField { get; set; }
        public float FloatField { get; set; }
        public double DoubleField { get; set; }
        public decimal DecimalField { get; set; }
        public char CharField { get; set; }
        public string StringField { get; set; }

        public override int GetHashCode() {
            return BoolField.GetHashCode() +
                   ByteField.GetHashCode() +
                   SbyteField.GetHashCode() +
                   ShortField.GetHashCode() +
                   UshortField.GetHashCode() +
                   IntField.GetHashCode() +
                   UintField.GetHashCode() +
                   LongField.GetHashCode() +
                   UlongField.GetHashCode() +
                   FloatField.GetHashCode() +
                   DoubleField.GetHashCode() +
                   DecimalField.GetHashCode() +
                   CharField.GetHashCode() +
                   (StringField?.GetHashCode() ?? 0);
        }
    }
}