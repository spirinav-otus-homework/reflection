﻿// ReSharper disable UnusedMember.Global

using System;

namespace ConsoleApp.Classes
{
    /// <summary>
    /// Сlass with datetime field
    /// </summary>
    public class ClassFieldDatetime
    {
        public DateTime DateTimeField;
    }
}