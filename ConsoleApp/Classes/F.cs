﻿namespace ConsoleApp.Classes
{
    public class F
    {
        public int I1;
        public int I2;
        public int I3;
        public int I4;
        public int I5;
        public F Get() => new() {I1 = 1, I2 = 2, I3 = 3, I4 = 4, I5 = 5};
    }
}