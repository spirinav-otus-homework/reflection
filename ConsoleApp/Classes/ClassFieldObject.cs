﻿using System.Collections.Generic;

namespace ConsoleApp.Classes
{
    /// <summary>
    /// Сlass with class in object field
    /// </summary>
    public class ClassFieldObjectClass
    {
        public object ObjectField = new ClassFieldPrivatePrimitive();
    }

    /// <summary>
    /// Сlass with collection in object field
    /// </summary>
    public class ClassFieldObjectCollection
    {
        public object CollectionField = new List<string> {"string vlue 1", "string value 2"};
    }

    /// <summary>
    /// Сlass primitive in object field
    /// </summary>
    public class ClassFieldObjectPrimitive
    {
        public object BoolField = true;
        public object ByteField = byte.MaxValue;
        public object SbyteField = sbyte.MaxValue;
        public object ShortField = short.MaxValue;
        public object UshortField = ushort.MaxValue;
        public object IntField = int.MaxValue;
        public object UintField = uint.MaxValue;
        public object LongField = long.MaxValue;
        public object UlongField = ulong.MaxValue;
        public object FloatField = float.MaxValue;
        public object DoubleField = double.MaxValue;
        public object DecimalField = decimal.MaxValue;
        public object CharField = char.MaxValue;
        public object StringField = "string value";
    }
}