﻿namespace ConsoleApp.Classes
{
    /// <summary>
    /// Сlass with public fields of primitive type
    /// </summary>
    public class ClassFieldPublicPrimitive
    {
        public bool BoolField;
        public byte ByteField;
        public sbyte SbyteField;
        public short ShortField;
        public ushort UshortField;
        public int IntField;
        public uint UintField;
        public long LongField;
        public ulong UlongField;
        public float FloatField;
        public double DoubleField;
        public decimal DecimalField;
        public char CharField;
        public string StringField;
    }
}