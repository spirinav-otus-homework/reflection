﻿// ReSharper disable UnusedMember.Global

using System.Globalization;

namespace ConsoleApp.Classes
{
    /// <summary>
    /// Сlass with custom nested class field
    /// </summary>
    public class ClassFieldNestedClassCustom
    {
        public ClassFieldCollectionBase NestedClass = new();
    }

    /// <summary>
    /// Сlass with system nested class field
    /// </summary>
    public class ClassFieldNestedClassSystem
    {
        public CultureInfo NestedClass = new("en-US");
    }
}