﻿namespace ConsoleApp.Classes
{
    /// <summary>
    /// Сlass with static fields of primitive type
    /// </summary>
    public class ClassFieldStaticPrimitive
    {
        private static bool BoolField = true;
        public static byte ByteField = byte.MaxValue;
        private static sbyte SbyteField = sbyte.MaxValue;
        public static short ShortField = short.MaxValue;
        private static ushort UshortField = ushort.MaxValue;
        public static int IntField = int.MaxValue;
        private static uint UintField = uint.MaxValue;
        public static long LongField = long.MaxValue;
        private static ulong UlongField = ulong.MaxValue;
        public static float FloatField = float.MaxValue;
        private static double DoubleField = double.MaxValue;
        public static decimal DecimalField = decimal.MaxValue;
        private static char CharField = '!';
        public static string StringField = "string value";
    }
}