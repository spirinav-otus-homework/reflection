﻿// ReSharper disable UnusedMember.Global
namespace ConsoleApp.Classes
{
    /// <summary>
    /// Сlass with struct field
    /// </summary>
    public class ClassFieldStruct
    {
        public SampleStruct StructField;
    }

    public struct SampleStruct
    {
        public int IntField;
        public string StringField;
    }
}