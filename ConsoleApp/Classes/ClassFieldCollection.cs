﻿// ReSharper disable UnusedMember.Global

using System.Collections.Generic;

namespace ConsoleApp.Classes
{
    /// <summary>
    /// Сlass with collection field
    /// </summary>
    public class ClassFieldCollectionBase
    {
        public string[] StringArray = {"string value 1", "string value 2"};
    }

    /// <summary>
    /// Derived class from class with collection field
    /// </summary>
    public class ClassFieldCollectionDerived : ClassFieldCollectionBase
    {
        public string StringField = "string value";
    }

    /// <summary>
    /// Сlass with list field
    /// </summary>
    public class ClassFieldList
    {
        public List<string> StringList = new() {"string value 1", "string value 2"};
    }

    /// <summary>
    /// Сlass with dictionary field
    /// </summary>
    public class ClassFieldDictionary
    {
        public Dictionary<int, string> StringList = new() {{1, "string value 1"}, {2, "string value 2"}};
    }
}