﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;

namespace Serializer
{
    public static class CsvSerializer
    {
        private const string Separator = ",";

        private static BindingFlags BindingFlags =>
            BindingFlags.DeclaredOnly | BindingFlags.Instance | BindingFlags.Static |
            BindingFlags.Public | BindingFlags.NonPublic;

        /// <summary>
        /// Serialize an object to a string in CSV format
        /// </summary>
        /// <param name="obj">Object to serialize</param>
        /// <returns>String in CSV format</returns>
        public static string Serialize(object obj)
        {
            var result = string.Empty;

            if (obj == null)
            {
                throw new ArgumentException("Object to serialize not specified");
            }

            var type = obj.GetType();

            if (!type.IsClass || type.GetInterface("IEnumerable") != null || type == typeof(object) ||
                type == typeof(string))
            {
                throw new SerializationException("Serializer only works with classes.");
            }

            var fieldInfoList = type.GetFields(BindingFlags).OrderBy(x => x.MetadataToken).ToList();

            var baseType = type.BaseType;
            while (baseType != null)
            {
                fieldInfoList.AddRange(baseType.GetFields(BindingFlags).OrderBy(x => x.MetadataToken));
                baseType = baseType.BaseType;
            }

            foreach (var fieldInfo in fieldInfoList)
            {
                var fieldValue = fieldInfo.GetValue(obj);
                var fieldValueType = fieldValue?.GetType();

                if (fieldValueType != null && fieldValueType != typeof(DateTime) && fieldValueType != typeof(decimal) &&
                    fieldValueType.IsValueType && !fieldValueType.IsPrimitive)
                {
                    throw new SerializationException("Serialization of not null nested struct is not provided.");
                }

                if (fieldValueType != null && fieldValueType != typeof(string) && fieldValueType.IsClass)
                {
                    throw new SerializationException("Serialization of not null nested class is not provided.");
                }

                if (fieldValueType != null && fieldValueType != typeof(string) &&
                    fieldValueType.GetInterface(nameof(IEnumerable)) != null)
                {
                    throw new SerializationException("Serialization of collections is not provided.");
                }

                switch (fieldValue)
                {
                    case float floatValue:
                        result += Separator + floatValue.ToString(new CultureInfo("en-US"));
                        continue;
                    case double doubleValue:
                        result += Separator + doubleValue.ToString(new CultureInfo("en-US"));
                        continue;
                    case string:
                    case char:
                        result += $"{Separator}\"{fieldValue}\"";
                        continue;
                }

                result += Separator + (fieldValue ?? "null");
            }

            return result[1..];
        }

        /// <summary>
        /// Deserialize a string in CSV format to a class T object
        /// </summary>
        /// <typeparam name="T">Class</typeparam>
        /// <param name="csvString">String in CSV format</param>
        /// <returns>Deserialized object</returns>
        public static T Deserialize<T>(string csvString) where T : class
        {
            if (string.IsNullOrWhiteSpace(csvString))
            {
                throw new ArgumentException("String to deserialize not specified");
            }

            var fieldInfoList = typeof(T).GetFields(BindingFlags).OrderBy(x => x.MetadataToken).ToList();

            var fieldValues = csvString.Split(Separator);

            if (fieldInfoList.Count != fieldValues.Length)
            {
                throw new SerializationException(
                    "The number of values is not equal to the number of fields of the target type.");
            }

            var result = Activator.CreateInstance(typeof(T));

            foreach (var fieldInfo in fieldInfoList)
            {
                var typeConverter = TypeDescriptor.GetConverter(fieldInfo.FieldType);
                var fieldValue = fieldValues[fieldInfoList.IndexOf(fieldInfo)];

                if (fieldValue == "null")
                {
                    fieldInfo.SetValue(result, null);
                    continue;
                }

                fieldInfo.SetValue(result, typeConverter.ConvertFromInvariantString((fieldValue.Replace("\"", ""))));
            }

            return (T) result;
        }
    }
}